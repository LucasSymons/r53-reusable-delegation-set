﻿# Setup AWS Connectons
Import-Module AWSPowerShell
Initialize-AWSDefaultConfiguration -ProfileName default -Region ap-southeast-2
#################################################^^^^^#######################
# Edit to match your local profile                                          #

# Domain name you wish to setup
$ZoneName = "Example.com"
$DelegationSetId = "N123123121I" #Reusable deligation set ID

# Logic
$zone = $ZoneName.Substring(0).ToLower() #check no wite space and all lower case.
$zoneprefix = $zone -replace '[.]','' #get domain with out dots, name used for logging
$date = get-date -Format ddMMyyyymmss # date for various tasks that require time stamps
Write-Output "$Zone"
Write-Output "$zoneprefix"
Write-Output "$date"
New-R53HostedZone -Name $Zone -CallerReference "$zoneprefix-$date" -HostedZoneConfig_Comment "Domain-$zoneprefix-$date" -DelegationSetId "$DelegationSetId"