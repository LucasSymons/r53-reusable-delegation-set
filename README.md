# R53-Reusable-Delegation-Set

Powershell scripts for setting up and using an AWS Route 53 Reusable Delegation set

1. Register first domain with out using a reusable delegation set
2. Run `Create-R53-reusabledelegationset.ps1` and imput the hosted zone ID for the doamin you setup in step 1. 
3. Run `Create-AWS-HostedZone.ps1` using the output of the first script to set the reusable delegation set ID. Then just add all the domains you wish you setup with that delegation set. This could be easily set to pull from a CSV and loop through lots of domains. 

## Reusable delegation sets can be used for a max of 100 hosted zones. 