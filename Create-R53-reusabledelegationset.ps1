﻿# Setup AWS Connectons
Import-Module AWSPowerShell
Initialize-AWSDefaultConfiguration -ProfileName default -Region ap-southeast-2
#################################################^^^^^#######################
# Edit to match your local profile                                          #

$Date = get-date -format yyyyMMddss
$SetName = "Nameservers-$date"
$HostedZoneId = "Z1F12123123G" #Must be hosted Zone ID of exisitng zone registered the normal way
Write-output "New-R53ReusableDelegationSet -HostedZoneId $HostedZoneId -CallerReference $SetName"
New-R53ReusableDelegationSet -HostedZoneId $HostedZoneId -CallerReference $SetName

# List all reusable delegation sets
Get-R53ReusableDelegationSets
